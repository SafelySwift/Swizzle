# Stats

> Note: All tests for performance will be done with the 'Hello World' program

## Source: Interpreter
- Overall Lines: 1744

### Lexer
- Lines: 307
- Lexing: 0.000629425048828125 seconds

### Parser
- Lines: 441
- Parsing: 0.000133037567138672 seconds

### Interpreting
- Lines: 591
- Executing: 0.000631093978881836

### Total
- Running: 0.00139355659 seconds

## Source: Compiler

### Bytecode
- Lines: 54

### Intermediate Representation Parser
- Lines: NaN
- Parsing: NaN seconds

### Compiler
- Lines: NaN
- Compiling: NaN seconds

### Virtual Machine
- Lines: 117
- Executing: NaN seconds

### Assembly Language Converter (x86)
- Lines: NaN
- Converting: NaN seconds
